<?php
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: index.php');
}
$sUserId = $_SESSION['sUserId'];

$sUniqueName = uniqid();
$target_file = "images/$sUniqueName"; // $target_dir . basename($_FILES["fileToUpload"]["name"]);
echo "<div>$target_file</div>";
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 99999999999 ) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
// if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
// && $imageFileType != "gif" ) {
//     echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
//     $uploadOk = 0;
// }
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        require_once __DIR__.'/connect.php';
        $stmt = $db->prepare('SELECT user_fk  FROM profile_images WHERE user_fk= :sUserId');
        $stmt->bindValue(':sUserId',  json_decode($sUserId));
        $stmt->execute();
        $aRow = $stmt->fetch();
        echo json_encode($aRow);
        if($aRow == false){
            $stmt = $db->prepare('INSERT INTO profile_images VALUES (null,:sUserId,:filePath,null)');
            $stmt->bindValue(':sUserId',  json_decode($sUserId));
            $stmt->bindValue(':filePath', $sUniqueName);
            $stmt->execute();
        }
        $stmt = $db->prepare('UPDATE profile_images SET url= :filePath WHERE user_fk= :userId');
        $stmt->bindValue(':userId', json_decode($sUserId)  );
        $stmt->bindValue(':filePath', $sUniqueName);
        $stmt->execute();

        header('Location: /instagram/profile.php');
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}