<?php
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: index.php');
}
$sUserId = $_SESSION['sUserId'];
$sTxtImgDescription = $_POST['txtImgDescription'];
$sImageLocation = $_POST['txtImageLocation'];
$sUniqueName = uniqid();
$target_file = "images/$sUniqueName"; // $target_dir . basename($_FILES["fileToUpload"]["name"]);
echo "<div>$target_file</div>";
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 99999999999 ) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
// if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
// && $imageFileType != "gif" ) {
//     echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
//     $uploadOk = 0;
// }
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        require_once __DIR__.'/connect.php';
        $stmt = $db->prepare("INSERT INTO images (id, title, url, user_fk, uploaded_date) 
        VALUES (null, 'TITLE_HERE', :filePath, :userId, :uploadedDate);
        INSERT INTO images_descriptions (id, image_fk, description) VALUES (null,LAST_INSERT_ID(),:sTxtImgDescription);
        INSERT INTO images_location(id, image_fk, location) VALUES (null,LAST_INSERT_ID(),:sImageLocation)");
        $stmt->bindValue(':userId', json_decode($sUserId));
        $stmt->bindValue(':filePath', $sUniqueName);
        $stmt->bindValue(':uploadedDate', null );
        $stmt->bindValue(':sTxtImgDescription', $sTxtImgDescription);
        $stmt->bindValue(':sImageLocation', $sImageLocation);
        $stmt->execute();

        echo "The file ".$sUniqueName. " has been uploaded.";
        header('Location: /instagram/dashboard.php');
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}