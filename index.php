<?php require_once 'login-signup-top.php'; ?>

<div id="index">
    <div class="aperture">
        <div class="diaphragm">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
<div class="indexText">Make it simple but significant</div>
    <div class="imageContainer"></div>
    <div class="mainContainer">
  
    <div class="flexButtonContainer">
           <button onClick="location.href='login.php'">LOG IN</button>
        <button onClick="location.href='signup.php'">SIGN UP</button>
    </div>
    </div>

        </div>
<?php
$sLinkToScript = '<script src="js/index.js"></script>';
require_once 'bottom.php';
?>