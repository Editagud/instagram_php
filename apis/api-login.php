<?php
require_once __DIR__.'/../connect.php';

ini_set('display_errors', 1);

$sEmail = $_POST['txtLoginEmail'] ?? '';
if(empty($sEmail)){sendResponse(0, __LINE__, 'The email is missing.'); }
if(!filter_var($sEmail, FILTER_VALIDATE_EMAIL)){sendResponse(0, __LINE__, 'Please enter a valid email address.'); }


$sPassword = $_POST['txtLoginPassword'] ?? '';
if( empty($sPassword) ){ sendResponse(0, __LINE__,  'The password is missing.');  }
if( strlen($sPassword) < 6 ){ sendResponse(0, __LINE__,  'The password has to be at least 6 characters.'); }
if( strlen($sPassword) > 50 ){ sendResponse(0, __LINE__,  'The password has to be no longer than 50 characters.'); }

try{
    $stmt = $db->prepare('SELECT id, user_name, password, active from users WHERE email = :sEmail');
    $stmt->bindValue(':sEmail', $sEmail);
    $stmt->execute();
    $aRow = $stmt->fetch();

    if($aRow == false){
        sendResponse(0, __LINE__, 'Your email is wrong. Please try again');
    }
$sRowPasswordValidate = $aRow->password;

        if (!password_verify($sPassword, $sRowPasswordValidate)){
            sendResponse(0, __LINE__, 'Password is not correct');
        }
    if ($aRow->active != 1) {sendResponse(0, __LINE__, 'The email is not activated');}

    $sUserId=json_encode($aRow->id);

    $sUserName=json_encode($aRow->user_name);

        session_start();
        $_SESSION['sUserId'] = $sUserId;
        $_SESSION['sUserName'] = $sUserName;


    sendResponse(1, __LINE__, 'Success');



} catch( PDOException $e){
    echo '{"status":0,"message":"cannot connect to database"}';

}
// **************************************************

function sendResponse($bStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$bStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'"}';
    exit;
}