<?php

require_once __DIR__.'/../connect.php';
// TODO: check that the front-end only passes numbers

$stmt = $db->prepare('SELECT images.id, emotions.emotion, emotions.user_fk, COUNT(emotions.emotion) as total 
FROM images 
JOIN emotions ON images.id = emotions.image_fk 
GROUP BY emotions.image_fk  , emotions.emotion
');
$stmt->execute();
$aRows = $stmt->fetchAll();
echo json_encode($aRows);
