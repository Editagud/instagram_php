<?php
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: index.php');
}
$sUserId = $_SESSION['sUserId'];
require_once __DIR__.'/../connect.php';

$iFollowingId = $_GET['followId'];

try{
    $stmt = $db->prepare('INSERT INTO followers(id, user_fk, follower_fk) VALUES (null ,:iUserId, :iFollowingId) ');
    $stmt->bindValue(':iUserId',  json_decode($sUserId));
    $stmt->bindValue(':iFollowingId', $iFollowingId);
    $stmt->execute();

    sendResponse(1, __LINE__, 'Success');

}catch(PDOException $ex){
    echo $ex;
}
// **************************************************

function sendResponse($bStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$bStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'"}';

}