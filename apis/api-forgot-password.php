<?php
require_once __DIR__.'/../connect.php';
$sEmail = $_GET['email'] ?? '';
if(empty($sEmail)){sendResponse(0, __LINE__, 'The email is missing.'); }
if(!filter_var($sEmail, FILTER_VALIDATE_EMAIL)){sendResponse(0, __LINE__, 'Please enter a valid email address.'); }


try{
    $stmt = $db->prepare('SELECT id  from users WHERE email = :sEmail');
    $stmt->bindValue(':sEmail', $sEmail);
    $stmt->execute();
    $aRow = $stmt->fetch();

    if($aRow == false){
        sendResponse(0, __LINE__, 'The email is not found in the Instagram.');
    }


// SUCCESS

$sURL = 'http://editagud.com/instagram/reset-password?email='.$sEmail;
$to = $sEmail;
$subject = "Reset password for Instagram";
$message = "Hey, please click this link $sURL to reset your password for Instagram";
mail($sEmail,$subject,$message);
sendResponse(1, __LINE__, 'Success, please check your email!');
header('Location: ../login');

} catch( PDOException $e){
    echo $e;
    //echo '{"status":0,"message":"cannot connect to database"}';

}

//*******************************************************

function sendResponse($bStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$bStatus.', "code": '.$iLineNumber.', "message": "'.$sMessage.'"}';
    exit;
};
