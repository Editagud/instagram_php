<?php
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: index.php');
}
$sUserId = $_SESSION['sUserId'];
require_once __DIR__.'/../connect.php';
$sImageId = $_GET['sImageId'];

try{
    $stmt = $db->prepare('DELETE FROM images WHERE images.id = :iImageId ');
    $stmt->bindValue(':iImageId',  $sImageId);
    $stmt->execute();
    sendResponse(1, __LINE__, 'Success');

}catch(PDOException $ex){
    echo $ex;
}
// **************************************************

function sendResponse($bStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$bStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'"}';
    exit;
}