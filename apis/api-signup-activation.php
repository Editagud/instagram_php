<?php
require_once __DIR__.'/../connect.php';

$sEmail = $_GET['email'];
$sActivationKey = $_GET['activationKey'];

//validate
try{
    $stmt = $db->prepare('SELECT  activation_key, active FROM users WHERE email= :sEmail');
    $stmt->bindValue(':sEmail', $sEmail);
    $stmt->execute();
    $aRows = $stmt->fetchAll();
    if(count($aRows)== 0 ){
        echo 'sorry no users';
        exit;
    }

    foreach($aRows as $aRow){
        if($sActivationKey  != $aRow->activation_key){
            sendResponse(0, __LINE__, 'sorry, can not activate');

        }
        $stmt = $db->prepare('UPDATE users SET active=1 WHERE email = :sEmail');
        $stmt->bindValue(':sEmail', $sEmail);
        $stmt->execute();
        header('Location: ../login.php');

    }

}
catch( PDOException $e){
    { sendResponse(0, __LINE__, 'Sorry something went wrong');  }

}
// **************************************************

function sendResponse($bStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$bStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'"}';
    exit;
}

header('Location: ../login');
