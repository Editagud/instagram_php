<?php
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: index.php');
}
$sUserId = $_SESSION['sUserId'];
require_once __DIR__.'/../connect.php';

$sImageId = $_GET['sImageId'] ;
if (empty($sImageId))
{
    sendResponse(0, __LINE__, 'sImageId is missing');
}
$sEmotion = $_GET['sEmotion'] ;

$stmt = $db->prepare('SELECT image_fk, user_fk FROM emotions WHERE image_fk = :iImageId and user_fk= :sUserId');
$stmt->bindValue('iImageId', $sImageId );
$stmt->bindValue(':sUserId',  json_decode($sUserId));
$stmt->execute();
$aRow = $stmt->fetch();

if($aRow == false){
    $stmt = $db->prepare('INSERT INTO emotions(image_fk, user_fk, emotion, timestamp) VALUES (:iImageId,:sUserId,:sEmotion,null)');
    $stmt->bindValue('iImageId', $sImageId );
    $stmt->bindValue(':sUserId',  json_decode($sUserId));
    $stmt->bindValue('sEmotion', $sEmotion );
    $stmt->execute();
}
$stmt = $db->prepare('UPDATE emotions SET emotion=:sEmotion,timestamp=null WHERE image_fk = :iImageId and user_fk= :sUserId');
$stmt->bindValue('iImageId', $sImageId );
$stmt->bindValue(':sUserId',  json_decode($sUserId));
$stmt->bindValue('sEmotion', $sEmotion );
$stmt->execute();



// **************************************************

function sendResponse($bStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$bStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'"}';
    exit;
}