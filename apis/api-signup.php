<?php
require_once __DIR__.'/../connect.php';
ini_set('display_errors', 1);

$sUsername = $_POST['txtSignupUsername'] ?? '';
if(empty($sUsername)){sendResponse(0, __LINE__, 'The username is missing.'); }


// validate name
$sName = $_POST['txtSignupName'] ?? '';
if(empty($sName)){sendResponse(0, __LINE__, 'The name is missing.'); }
if(strlen($sName) < 2 ){ sendResponse(0, __LINE__, 'The name has to be at least 2 characters.'); }
if(strlen($sName) > 20 ){ sendResponse(0, __LINE__, 'The name has to be no longer than 20 characters.'); }

$sLastName = $_POST['txtSignupLastName'] ?? '';
if(empty($sLastName)){sendResponse(0, __LINE__ , 'The last name is missing.'); }
if(strlen($sLastName) < 2 ){ sendResponse(0, __LINE__, 'The last name has to be at least 2 characters.'); }
if(strlen($sLastName) > 20 ){ sendResponse(0, __LINE__, 'The last name has to be no longer than 20 characters.'); }

$sEmail = $_POST['txtSignupEmail'] ?? '';
if(empty($sEmail)){sendResponse(0, __LINE__, 'The email is missing.'); }
if(!filter_var($sEmail, FILTER_VALIDATE_EMAIL)){sendResponse(0, __LINE__, 'Please enter a valid email address.'); }

$sPassword = $_POST['txtSignupPassword'] ?? '';
if(empty($sPassword)){sendResponse(0, __LINE__,  'The password is missing.'); }
if(strlen($sPassword) < 6 ){ sendResponse(0, __LINE__,  'The password has to be at least 6 characters.'); }
if(strlen($sPassword) > 50 ){ sendResponse(0, __LINE__,  'The password has to be no longer than 50 characters.'); }
$sPasswordConfirm = $_POST['txtSignupPasswordConfirm'] ?? '';
if(empty($sPasswordConfirm)){sendResponse(0, __LINE__,  'The password confirm is missing.'); }
if( $sPasswordConfirm !== $sPassword){ sendResponse(0, __LINE__,  'The password and the password confirm is not matching.'); }

$sActivationKey =uniqid();
try{
    $stmt = $db->prepare("INSERT INTO users VALUES (null, :sUserName,  :sPassword,  :sName, :sLastName, :sEmail , :iActive, :sActivationKey)");
    $stmt->bindValue(':sUserName', $sUsername);
    $stmt->bindValue(':sPassword', password_hash($sPassword, PASSWORD_DEFAULT));
    $stmt->bindValue(':sName', $sName);
    $stmt->bindValue(':sLastName', $sLastName);
    $stmt->bindValue(':sEmail', $sEmail);
    $stmt->bindValue(':iActive', 0);
    $stmt->bindValue(':sActivationKey', $sActivationKey);
    $stmt->execute();

    $sURL = 'http://editagud.com/instagram/apis/api-signup-activation.php?email='.$sEmail.'&activationKey='.$sActivationKey;
    $to = $sEmail;
    $subject = "User activation for INSTAGRAM";
    $message = "Hey, please click this link $sURL for activate your user for Instagram";
    mail($to,$subject,$message);
    { sendResponse(1, __LINE__, 'Success');  }


} catch( PDOException $e){
  echo $e;
    exit();
}
// **************************************************

function sendResponse($bStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$bStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'"}';
    exit;
}
