<?php
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: index.php');
}
require_once __DIR__.'/../connect.php';
$sUserId = $_SESSION['sUserId'];
$sSearchName = $_GET['textSearch'] ?? '';



try{
    $stmt = $db->prepare('SELECT users.id, users.user_name,  users.first_name, users.last_name, profile_images.url  FROM users
LEFT JOIN profile_images ON users.id = profile_images.user_fk 
WHERE user_name like :sSearchName AND NOT users.id= :sUserId 
or first_name like :sSearchName AND NOT users.id= :sUserId 
or last_name like :sSearchName AND NOT users.id= :sUserId ');
    $stmt->bindValue(':sSearchName', "%$sSearchName%");
    $stmt->bindValue(':sUserId', json_decode($sUserId));
    $stmt->execute();
    $aRows = $stmt->fetchAll();
    if(count($aRows)== 0 ){
        echo 'sorry no users';
        exit;
    }
    foreach($aRows as $aRow) {
        $iFollowerId = json_decode($aRow->id);
        echo '<div class="searchDiv" >
                    <div class="flexBox">
                   <img  class="searchImages" src="images/'.(($aRow->url == 0) ?  'profile.png' : $aRow->url).'"/>
                   <div class="FollowBox" >
                     <div class="goToAnotherUserPage" id="'.$iFollowerId.'" ><h3 id="'.$iFollowerId.'" >' . $aRow->user_name . ' </h3 class="userName">  <h5 id="'.$iFollowerId.'">' . $aRow->first_name . '  ' . $aRow->last_name . '</h5> </div> ';



        $followersQuery = $db->prepare('SELECT user_fk, follower_fk FROM followers WHERE user_fk = :sUserId AND follower_fk= :iFollowerId');
        $followersQuery->bindValue(':sUserId', json_decode($sUserId));
        $followersQuery->bindValue(':iFollowerId',$iFollowerId);
        $followersQuery->execute();
        $aFollower = $followersQuery->fetch();

        if($aFollower == false){

            echo '     <button type="button" class="followBtn" id="'.$iFollowerId.'" data-name="' . $aRow->user_name . '"> + Follow </button> 
                    </div> 
                    </div>
                       </div>';
        }else{
            echo'</div> </div></div>';
        }

               }

}catch(PDOException $ex){
    echo $ex;
}
$sLinkToScript = "<script src='../js/search.js'> ";
