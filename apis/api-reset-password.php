<?php
require_once __DIR__.'/../connect.php';

$sEmail = $_POST['txtEmail'] ?? '';
if(empty($sEmail)){sendResponse(0, __LINE__, 'The email is missing.'); }
if(!filter_var($sEmail, FILTER_VALIDATE_EMAIL)){sendResponse(0, __LINE__, 'Please enter a valid email address.'); }

$sPassword = $_POST['txtNewPassword'] ?? '';
if(empty($sPassword)){sendResponse(0, __LINE__,  'The password is missing.'); }
if(strlen($sPassword) < 6 ){ sendResponse(0, __LINE__,  'The password has to be at least 6 characters.'); }
if(strlen($sPassword) > 50 ){ sendResponse(0, __LINE__,  'The password has to be no longer than 50 characters.'); }
$sPasswordConfirm = $_POST['txtNewPasswordConfirm'] ?? '';
if(empty($sPasswordConfirm)){sendResponse(0, __LINE__,  'The password confirm is missing.'); }
if( $sPasswordConfirm !== $sPassword){ sendResponse(0, __LINE__,  'The password and the password confirm is not matching.'); }

try{
    $stmt = $db->prepare("UPDATE users SET users.password = :sPassword WHERE users.email = :sEmail");
    $stmt->bindValue(':sPassword', password_hash($sPassword, PASSWORD_DEFAULT));
    $stmt->bindValue(':sEmail', $sEmail);
    $stmt->execute();




    { sendResponse(1, __LINE__, 'Success');  }


} catch( PDOException $e){
    { sendResponse(0, __LINE__, 'Sorry something went wrong');  }
    exit();
}
//*******************************************************

function sendResponse($bStatus, $iLineNumber, $sMessage){
    echo '{"status":'.$bStatus.', "code":'.$iLineNumber.', "message": "'.$sMessage.'"}';
    exit;
}
