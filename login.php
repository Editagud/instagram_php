<?php require_once 'login-signup-top.php'; ?>

    <div class="centeredForms">
    <h3>WELCOME BACK</h3>
     <h4>Sign in with your account</h4>
        <p style="margin-top: 10px;opacity: 0.5;">For exploring use </br>email: example@example.com </br> password: password</p>
    <form id="frmLogin">
        <input type="text" name="txtLoginEmail" placeholder="email"  data-validate="yes" data-type="email">
        <input type="password" name="txtLoginPassword" placeholder="password"   data-validate="yes" data-type="string" data-min="6" data-max="50" >
        <button>LOGIN</button>
        <div>
        <a class="navLink" id="myBtn">Forgot your password?</a>
        </div>
    </form>
        <div id='forgotPasswordModal' class="modal">
            <div class='modal-content'>
                <span class="close">&times;</span>
                <h3>Forgot password?</h3>
                <form id="frmForgotPassword" >
                    Enter your email. We'll email instructions on how to reset your password.
                    <input name="txtForgotPasswordEmail" id="txtForgotPasswordEmail" type="text" placeholder="Your email" data-validate="yes" data-type="email" d>

                            <button type="submit"  name="submit">Send</button>
                        </div>
                    </div>
                </form>
            </div>
    <div class="center"> <a class="backButton" href="index.php"> <<< Back to home</a></div>
        </div>


    <div>
<?php
$sLinkToScript = '<script src="js/login.js"></script>';
require_once 'bottom.php';
?>