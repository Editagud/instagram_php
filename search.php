<?php
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: index.php');
}
$sUserId = $_SESSION['sUserId'];
$sUserName = $_SESSION['sUserName'];
require_once __DIR__.'/connect.php';

$sInjectCss = '<link rel="stylesheet" href="css/dashboard.css">';

require_once __DIR__.'/top.php';
?>



<div class="pageSearch">
    <nav class="subnavSearch">
        <button class="navLink active"  data-showPage="search">People</button>
        <button class="navLink"  data-showPage="searchHashtags"># Tags</button>
    </nav>
    <div id="search" class="page">
        <div class="searchContainer">
            <form id="frmSearch">
                <input type="text" name="textSearch" id="textSearch" placeholder="search people">
                <div id="nameList"></div>
                <svg class='searchIcon' width="25" height="25" viewBox="0 0 1792 1792"><path d="M1216 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/></svg>
            </form>
    </div>
    </div>
    <div id="searchHashtags" class="page">
        <div class="searchContainer">
            <form id="frmSearchHashtags">
                <input type="text" name="textSearchTag" id="textSearchTag" placeholder="search by #">

                <svg class='searchIcon' width="25" height="25" viewBox="0 0 1792 1792"><path d="M1216 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"/></svg>
                <div id="HashtagsList" class="imagesListDisplay"></div>
            </form>

        </div>

    </div>
    <div>

<?php
$sLinkToScript = "<script src='js/search.js'> </script>";
require_once __DIR__.'/bottom.php';