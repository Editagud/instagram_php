// Get the modal
var modal = document.getElementById("avatarModal");
// Get the button that opens the modal
var btn = document.getElementById("myBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks on the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
};
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

/**********************************************/

$(document).ready(function() {
  getEmotions();
});
$("svg").on("click", function() {
  window.location.reload();
});

$(".deleteContainer").on("click", function(e) {
  let sImageId = e.target.getAttribute("id");
console.log(sImageId)
  $.ajax({
    method: "GET",
    url: "apis/api-delete-image.php",
    data: {
      sImageId: sImageId
    },
    dataType: "JSON",
    cache: false
  })
    .done(function(jData) {
      if (jData.status == 1) {
        swal({
          title: "POOF!",
          text: "The image has been deleted",
          icon: "success"
        }).then(function() {
          window.location.reload();
        });
      } else {
        swal({
          text: jData.message,
          icon: "warning"
        });
      }
    })
    .fail(function() {});
});

function setEmotion(e, sEmotion) {
  let oParent = e.parentElement;
  let aSvgs = oParent.querySelectorAll("svg");
  for (var i = 0; i < aSvgs.length; i++) {
    if (aSvgs[i] != e) {
      aSvgs[i].classList.remove("green");
    }
  }
  e.classList.toggle("green");
  let sImageId = oParent.getAttribute("data-profile-image-id");
  console.log(e.parentElement);
  console.log(sImageId);
  console.log(sEmotion);
  console.log(e);

  $.ajax({
    method: "GET",
    url: "apis/api-set-emotion.php",
    data: {
      sImageId: sImageId,
      sEmotion: sEmotion
    },
    dataType: "JSON",
    cache: false
  })
    .done(function(ajData) {
      console.log(ajData);
    })
    .fail(function() {});
}

function getEmotions() {
  $.ajax({
    method: "GET",
    url: "apis/api-get-emotions.php",
    data: {},
    dataType: "JSON",
    cache: false
  })
    .done(function(Emotions) {
      console.log(Emotions);
      for (var i = 0; i < Emotions.length; i++) {
        var sImageId = Emotions[i].id;
        // console.log(sImageId)
        // $(`[data-image-id=${sImageId}]`).hide()
        var sEmotion = Emotions[i].emotion; // 0 1 2 3
        var sTotal = Emotions[i].total;
        $("[data-profile-image-id=" + sImageId + "]")
          .find("#" + sEmotion)
          .text(sTotal);
      }
    })
    .fail(function() {});
}
function previewImage() {
  let preview = document.querySelector("#thumbnail");
  let file = document.querySelector("input[type=file]").files[0];
  let reader = new FileReader();
  reader.onloadend = function() {
    preview.src = reader.result;
  };
  reader.readAsDataURL(file);
}


