$(document).ready(function(){
    getEmotions()
});
$('svg').on('click', function(){
    window.location.reload()
});



function setEmotion( e, sEmotion ){
    let oParent = e.parentElement
    let aSvgs = oParent.querySelectorAll('svg')
    for(var i = 0; i < aSvgs.length; i++ ){
        if( aSvgs[i] != e ){
            aSvgs[i].classList.remove('green')
        }
    }
    e.classList.toggle('green')
    let sImageId = oParent.getAttribute('data-user-image-id')
    console.log( e.parentElement );
    console.log( sImageId );
    console.log(sEmotion);
    console.log(e)

    $.ajax({
        method:"GET",
        url:"apis/api-set-emotion.php",
        data:{
            "sImageId":sImageId,
            "sEmotion":sEmotion
        },
        dataType:"JSON",
        cache: false
    }).
    done(function(ajData){
        console.log(ajData)

    }).
    fail(function(){

    })
}


function getEmotions(){


    $.ajax({
        method:"GET",
        url:"apis/api-get-emotions.php",
        data:{},
        dataType:"JSON",
        cache: false
    }).done(function(Emotions){

        console.log(Emotions)
        for(var i = 0; i < Emotions.length; i++){
            var sImageId = Emotions[i].id
            // console.log(sImageId)
            // $(`[data-image-id=${sImageId}]`).hide()
            var sEmotion = Emotions[i].emotion // 0 1 2 3
            var sTotal = Emotions[i].total
            $('[data-user-image-id='+sImageId+']').find('#'+ sEmotion).text(sTotal)



        }
    }).
    fail(function(){

    })
}
$(".followBtn").on("click", function(e) {
    let followId = e.target.getAttribute("id");
    console.log(followId)

    followingName = e.target.getAttribute('data-name');

    console.log(followingName);
    $.ajax({
        method: "GET",
        url: "apis/api-set-follow.php",
        data: {
            "followId": followId
        },
        dataType: "JSON",
        cache: false
    })
        .done(function(res) {
            if (res.status == 1) {
                swal({
                    title: "BAM!",
                    text: "You are following " + followingName,
                    icon: "success"
                }).then(function() {
                    window.location.reload();
                });
            }
        })
        .fail(function(e) {
            console.log(e);
        });
});
$(".unfollowBtn").on("click", function(e) {
    let followId = e.target.getAttribute("id");
    console.log(followId)
    followingName = e.target.getAttribute('data-name');

    console.log(followingName);
    $.ajax({
        method: "GET",
        url: "apis/api-delete-follow.php",
        data: {
            "followId": followId
        },
        dataType: "JSON",
        cache: false
    })
        .done(function(res) {
            if (res.status == 1) {
                swal({
                    title: "😣",
                    text: "You are not following, " + followingName + ", anymore...",
                    icon: "success"
                }).then(function() {
                    window.location.reload();
                });
            }
        })
        .fail(function(e) {
            console.log(e);
        });
});