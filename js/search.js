$(".page").hide(); //Hide all content
$(".page:first").show(); //Show first tab content

$("nav>button").click(function() {
  $("button").removeClass("active"); // remove the active
  $(".page").hide(); // hide all pages
  $(this).addClass("active");
  let sPageToShow = $(this).attr("data-showPage"); // get name of page to show
  $("#" + sPageToShow).show(); // show page by # ID
});

$("#frmSearch").on("input", "#textSearch", function(e) {
  e.preventDefault();
  $.ajax({
    method: "GET",
    url: "apis/api-search.php?textSearch=" + e.target.value,
    ceche: false
  })
    .done(function(data) {
      $("#nameList").html(data);
      $(".followBtn").on("click", function(e) {
        let followId = e.target.getAttribute("id");
        console.log(followId)

        followingName = e.target.getAttribute('data-name');

        console.log(followingName);
        $.ajax({
          method: "GET",
          url: "apis/api-set-follow.php",
          data: {
            followId: followId
          },
          dataType: "JSON",
          cache: false
        })
          .done(function(res) {
            if (res.status == 1) {
              swal({
                title: "BAM!",
                text: "You are following " + followingName,
                icon: "success"
              }).then(function() {
                window.location.reload();
              });
            }
          })
          .fail(function(e) {
            console.log(e);
          });
      });
      $(".goToAnotherUserPage").on("click",  function(event) {

        let anotherUserId = event.target.getAttribute("id");

          location.href="/instagram/user-profile.php?sAnotherUserId="+ anotherUserId;



      });

    })
    .fail(function() {
      console.log("error");
    });
});

$("#frmSearchHashtags").on("input", "#textSearchTag", function(e) {
  e.preventDefault();
  $.ajax({
    method: "GET",
    url: "apis/api-search-by-hashtags.php?textSearchTag=" + e.target.value,
    ceche: false
  })
    .done(function(data) {
      $("#HashtagsList").html(data);
      getEmotions();
    })
    .fail(function() {
      console.log("error");
    });
});

function setEmotion(e, sEmotion) {
  let oParent = e.parentElement;
  let aSvgs = oParent.querySelectorAll("svg");
  for (var i = 0; i < aSvgs.length; i++) {
    if (aSvgs[i] != e) {
      aSvgs[i].classList.remove("green");
    }
  }
  e.classList.toggle("green");
  let sImageId = oParent.getAttribute("data-search-image-id");
  console.log(e.parentElement);
  console.log(sImageId);
  console.log(sEmotion);
  console.log(e);

  $.ajax({
    method: "GET",
    url: "apis/api-set-emotion.php",
    data: {
      sImageId: sImageId,
      sEmotion: sEmotion
    },
    dataType: "JSON",
    cache: false
  })
    .done(function(ajData) {
      console.log(ajData);
    })
    .fail(function() {});
}

function getEmotions() {
  $.ajax({
    method: "GET",
    url: "apis/api-get-emotions.php",
    data: {},
    dataType: "JSON",
    cache: false
  })
    .done(function(Emotions) {
      console.log(Emotions);
      for (var i = 0; i < Emotions.length; i++) {
        var sImageId = Emotions[i].id;
        var sEmotion = Emotions[i].emotion; // 0 1 2 3
        var sTotal = Emotions[i].total;
        $("[data-search-image-id=" + sImageId + "]")
          .find("#" + sEmotion)
          .text(sTotal);
      }
    })
    .fail(function() {});
}
