$("#frmResetPassword").submit(function() {
    $.ajax({
        method: "POST",
        url: "apis/api-reset-password.php",
        data: $("#frmResetPassword").serialize(),
        dataType: "JSON"
    })
        .done(function(jData) {
            console.log(jData);
            if (jData.status == 1) {
                swal({
                    title: "SUCCESS",
                    text: "You have changed your password",
                    icon: "success"
                });
                location.href = "login.php";
            } else {
                swal({
                    title: "Something went wrong.",
                    text: jData.message,
                    icon: "warning"
                });
            }
        })
        .fail(function() {
            console.log("error");
        });

    return false;
});
