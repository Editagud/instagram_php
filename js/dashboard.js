// Get the modal
var modal = document.getElementById("imageUploadModal");

var btn = document.getElementById("uploadButton");

var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
  modal.style.display = "block";
};

span.onclick = function() {
  modal.style.display = "none";
};

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

/***************************************************/

function previewImage() {
  let preview = document.querySelector("#thumbnail");
  let file = document.querySelector("input[type=file]").files[0];
  let reader = new FileReader();
  reader.onloadend = function() {
    preview.src = reader.result;
  };
  reader.readAsDataURL(file);
}
/**************************LOAD MORE ON SCROLL******************************** */

$(document).ready(function() {
  var start = 0;
  $.ajax({
    method: "GET",
    url: "apis/api-get-data.php",
    data: {
      offset: 0,
      limit: 10
    }
  })
    .done(function(data) {
      console.log(data);
      $("#profileData").append(data);
      start += 10;
    })
    .fail(function() {});

  $(window).scroll(function() {
    if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
      $.ajax({
        method: "GET",
        url: "apis/api-get-data.php",
        data: {
          offset: start,
          limit: 10
        }
      })
        .done(function(data) {
          console.log(data);
          $("#profileData").append(data);
          start += 10;
        })
        .fail(function() {});
    }
  });
});
