$("#frmLogin").submit(function() {
    $.ajax({
        method: "POST",
        url: "apis/api-login.php",
        data: $("#frmLogin").serialize(),
        dataType: "JSON"
    })
        .done(function(jData) {
            if (jData.status == 0) {
                console.log(jData.message);
                swal({
                    title: "Something went wrong.",
                    text: jData.message,
                    icon: "warning"
                });
                return;
            }
            // SUCCESS
           console.log("Success")
            location.href = "dashboard.php";

        })
        .fail(function(err) {
            console.log("error");
            console.error(err);
        });

    return false;
});
// Get the modal
var modal = document.getElementById("forgotPasswordModal");
// Get the button that opens the modal
var btn = document.getElementById("myBtn");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks on the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
};
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
};
$("#frmForgotPassword").submit(function() {
    console.log($("#txtForgotPasswordEmail").val())
    $.ajax({
        method: "GET",
        url: "apis/api-forgot-password.php",
        data: {
            "email": $("#txtForgotPasswordEmail").val()
        },
        cache: false,
        dataType: "JSON"
    })
        .done(function(jData) {
            if (jData.status == 0) {
                swal({
                    title: "Something went wrong.",
                    text: jData.message,
                    icon: "warning"
                });
            }
            if (jData.status == 1) {
                swal({
                    title: "CONGRATS",
                    text: "Please check your email to reset your password.",
                    icon: "success"
                }).then(function() {
                    window.location = "login";
                });
            }
        })
        .fail(function() {
            console.log("FATAL ERROR");
        });

    return false;
});
