$("#frmSignup").submit(function() {
    $.ajax({
        method: "POST",
        url: "apis/api-signup.php",
        data: $("#frmSignup").serialize(),
        dataType: "JSON"
    })
        .done(function(jData) {
            console.log(jData);
            if (jData.status == 1) {
                swal({
                    title: "SUCCESS",
                    text: "You have signed up! Please check your email to activate user.",
                    icon: "success"
                }).then(function() {
                    window.location = "login";
                });
            } else {
                swal({
                    text: jData.message,
                    icon: "warning"
                });
            }
        })
        .fail(function() {
            console.log("error");
        });

    return false;
});
