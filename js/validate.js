$("form").submit(function() {
    // How do I know which elements in the form I want to validate?
    // How do I select all the data-validate="yes" from the form
    // let bErrors = false

    $(".error").remove();
    $(this)
        .find("input[data-validate=yes]")
        .each(function() {
            $(this).removeClass("invalid");
            // Ok, we have the input box... how do I know what to check for?
            let sDataType = $(this).attr("data-type");
            let iMin = $(this).attr("data-min");
            let iMax = $(this).attr("data-max");
            let iLen = $(this).attr("data-len");

            switch (sDataType) {
                case "string":
                    if ($(this).val().length < iMin || $(this).val().length > iMax) {
                        $(this)
                            .addClass("invalid")
                            .after(
                                '<span class="error">the field must be ta least ' +
                                iMin +
                                " but no longer that " +
                                iMax +
                                " characters.</span>"
                            );
                        // bErrors = true
                    }
                    break;
                case "integer":
                    if (!$.isNumeric($("input:text").val())) {
                        $(this)
                            .addClass("invalid")
                            .after(
                                '<span class="error">This field must contain only digits. </span>'
                            );
                    }
                    if (
                        Number($(this).val().length) < iMin ||
                        Number($(this).val().length) > iMax
                    ) {
                        $(this)
                            .addClass("invalid")
                            .after(
                                '<span class="error">the field must be ' +
                                iMin +
                                " long.</span>"
                            );
                    }
                    if (Number($(this).val().length) != iLen) {
                        $(this)
                            .addClass("invalid")
                            .after(
                                '<span class="error">This field must contain ' +
                                iLen +
                                " characters. </span>"
                            );
                    }

                    break;

                case "email":
                    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (re.test(String($(this).val()).toLowerCase()) == false) {
                        $(this)
                            .addClass("invalid")
                            .after(
                                '<span class="error">Please enter a valid email address. </span>'
                            );
                    }
                    break;

                default:
                    console.log("sorry, dont know how to validate that");
                    break;
            }
        });

    // if(bErrors==false){ return true }
    if (
        $(this)
            .children()
            .hasClass("invalid")
    ) {
        return false;
    }
    // chaining . . . chaining
});

// function validateEmail(email) {
//   var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//   return re.test(String(email).toLowerCase());
// }
// console.log( validateEmail('dfgfgffggffggf@gmail.com') )
