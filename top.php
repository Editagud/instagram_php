
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css">
    <?= $sInjectCss ?? '' ?>

    <title>INSTAGRAM <?= json_decode($_SESSION['sUserName']) ?></title>
</head>
<body class=<?= isset($_COOKIE["theme"]) ? $_COOKIE["theme"] : "light" ?> >

<nav>
    <div>
        <a href="dashboard.php" id="logoNav">  <div class="aperture logo">
                <div class="diaphragm">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div> <span id="logoName"> INSTAGRAM </span> <span class="pullRight"><?= json_decode($_SESSION['sUserName'])?></span></a>
    </div>
     <div class="theme-toggle" id="switchButton">
        <input id="theme" type="checkbox" <?= isset($_COOKIE["theme"]) ? ($_COOKIE["theme"] == "light" ? "" : "checked") : "" ?> >
        <label for="theme">  </label>
        <span class="on">🌙</span>
        <span class="off">☀️</span>
    </div>
    <div>
       

        <div id="mobile-menu">
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&#9776;</a>
                <div class="nav-links">
                <a href="search.php" onclick="closeNav()">Search </a>
                <a href="profile.php" onclick="closeNav()">Profile</a>
                <a href="logout.php"onclick="closeNav()">Log out</a>
                  
                </div>
            </div>

            <span id="openNav" onclick="openNav() ">&#9776;</span>
        </div>
        <header>
            <div class="header-nav">
             
                <a href="search.php"><i class=" fas fa-search fa-2x"></i> </a>
                <a href="profile.php"><i class="far fa-user-circle fa-2x"></i></a>
                <a href="logout.php">LOG OUT</a>
                    
                
            </div>
            </header>
    </div>
 
</nav>