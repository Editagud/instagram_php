<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://unpkg.com/js-cookie@2.2.0/src/js.cookie.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="js/validate.js"></script>

<script>
    function openNav() {
    document.getElementById("mySidenav").style.height = "100%";
    document.getElementById("openNav").classList.add("hidden");

}

function closeNav() {
    document.getElementById("mySidenav").style.height = "0";
    document.getElementById("openNav").classList.remove("hidden");

}
    document.querySelector("#theme").addEventListener("change", function() {
        const nextTheme = Cookies.get("theme") === "light" ? "dark" : "light"
        document.body.classList.toggle("light");
        document.body.classList.toggle("dark");
        Cookies.set("theme", nextTheme);
    });
</script>
<?= $sLinkToScript ?? ''; ?>
</body>
</html>