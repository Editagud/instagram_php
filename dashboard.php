<?php
session_start();
if( !isset($_SESSION['sUserId'] ) ){
    header('Location: index.php');
}
$sUserId = $_SESSION['sUserId'];
$sUserName = $_SESSION['sUserName'];
require_once __DIR__.'/connect.php';

$sInjectCss = '<link rel="stylesheet" href="css/dashboard.css">';

require_once __DIR__.'/top.php';

?>

    <div class="page" id="profile">
        <div class="center"><button id="uploadButton" class="addButton">Add image</button></div>
        <div id='imageUploadModal' class="modal">
            <div class='modal-content'>
                <span class="close">&times;</span>
                <h3>POST IMAGE</h3>
                <form action="upload.php" method="post" enctype="multipart/form-data">
                    Select image to upload:
                    <input type="file" name="fileToUpload" id="fileToUpload" onchange="previewImage()">
                    <input name="txtImageLocation" placeholder="Location?">
                    <textarea name="txtImgDescription" placeholder="Say something..."></textarea>
                    <div class="modalPostView">
                    <img id="thumbnail" class="thumbnail" src="" alt="preview..." />
                    <div>
                    <button type="submit"  name="submit">Upload Image</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>

        <div id="profileData" class="imagesListDisplay">

       
            </div>
         
           
            
        </div>
<?php
$sLinkToScript = "<script src='js/dashboard.js'> </script>";
require_once __DIR__.'/bottom.php';
